## :woman_cartwheeling: Bienvenue

<p align="left">
<a href="https://twitter.com/SimonetLa3" target="blank"><img align="center" src="Icon/twitter.png" title = "Twitter" alt="" height="30" /></a>
<a href="www.linkedin.com/in/léa-simonet-884615172" target="blank"><img align="center" src="Icon/linkedin.png" alt="" height="30" /></a>
<a href="http://instagram.com/sim6.photos" target="blank"><img align="center" src="https://github.com/mishmanners/MishManners/blob/master/socials/instagram.png" alt="" height="30" /></a>
<a href="mailto:simonetlea97@gmail.com" target="blank"><img align="center" src="Icon/gmail.png" alt="" height="30" /></a>
    
</p>

### Ma playlist 🎵

<a href="https://open.spotify.com/user/11173087224?si=583b1463f6ca425f" target="blank"><img align="center" src="Icon/spotify.png" alt="" height="30" /></a>
    
<p style="text-align: justify;">
    Bienvenue sur mon profil GitHub ! Je suis un développeur passionné issu d'une filière biologie BAC +5 avec une solide formation en statistiques. Au fil du temps, j'ai découvert que l'informatique était un domaine fascinant qui pouvait me permettre de concilier mes compétences analytiques et mon désir de créer des solutions pratiques et efficaces.

C'est pourquoi j'ai décidé de me lancer dans une reconversion en tant qu'ingénieur développeur Java. Depuis, j'ai acquis une solide expérience dans le développement de logiciels et d'applications, en travaillant sur des projets variés et stimulants.

Mon profil GitHub est l'endroit où vous pourrez découvrir mes projets les plus récents, ainsi que mes contributions à diverses communautés de développement open source. J'ai hâte de partager mes connaissances et d'apprendre de nouvelles choses en travaillant avec d'autres développeurs talentueux.

N'hésitez pas à parcourir mes projets et à me contacter si vous avez des questions ou des idées de collaboration. Je suis toujours à la recherche de nouveaux défis et de nouvelles opportunités de contribuer à l'évolution de la technologie.
</p>

### :point_right: Compétences
#### Langages / Frameworks
<img src="./Icon/java.png" alt="java" title="Java"/>&nbsp;&nbsp; 
<img src="./Icon/javascript.png" alt ="Javascript" title="Javascript"/>&nbsp;&nbsp; 
<img src="./Icon/angular.png" alt ="angular" title="Angular"/>&nbsp;&nbsp;

### :point_right: Outils
<img src="./Icon/github.png" alt ="GitHub" title="GitHub"/>&nbsp;&nbsp; 
<img src="./Icon/git.png" alt ="Git" title="Git"/>&nbsp;&nbsp; 
<img src="./Icon/r.png" alt ="R" title="R"/>&nbsp;&nbsp; 
<img src="./Icon/rstudio.png" alt ="Rstudio" title="Rstudio"/>&nbsp;&nbsp; 


| Quelques statistiques | | |
| :---: |:---:| :---:|
| ![](https://github-readme-stats.vercel.app/api/top-langs/?username=LeaSimonet&theme=radical&hide_langs_below=8&count_private=true)     |  | ![](https://github-readme-stats.vercel.app/api?username=LeaSimonet&show_icons=true&theme=radical&count_private=true) |

<details>
    <summary>
        CV détaillé
    </summary>

## EXPERIENCE PROFESSIONNELLE
### Formation Ingénieur Java junior 

## FORMATION
### 2022 – Master DyNEA – Université de Pau et des Pays de l'Adour - Anglet
Diplôme en dynamique des écosystèmes aquatiques.

### 2020 – Licence Biologie des organismes – Université de Pau et des Pays de l'Adour - Anglet
Diplôme de biologie - des milieux aquatiques.

### 2014 – BAC S – Lycée St Louis Villa Pia
Filière générale option physique chimmie.

### LANGUES
🇫🇷 🇬🇧 

### LOISIRS
Randonnée, ...
</details>

### Regardez mon graphique de contribution se faire manger par le serpent 🐍
![snake gif](https://github.com/LeaSimonet/LeaSimonet/blob/output/github-contribution-grid-snake.gif)
 

